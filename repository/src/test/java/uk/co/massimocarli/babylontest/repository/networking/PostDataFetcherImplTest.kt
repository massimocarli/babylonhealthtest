package uk.co.massimocarli.babylontest.repository.networking

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import uk.co.massimocarli.babylontest.networking.DataFetcher
import uk.co.massimocarli.babylontest.repository.CommentRepository
import uk.co.massimocarli.babylontest.repository.PostRepository
import uk.co.massimocarli.babylontest.repository.UserRepository
import uk.co.massimocarli.babylontest.repository.db.entity.Comment
import uk.co.massimocarli.babylontest.repository.db.entity.Post
import uk.co.massimocarli.babylontest.repository.db.entity.User


class PostDataFetcherImplTest {

  lateinit var api: Api
  lateinit var postRepo: PostRepository
  lateinit var userRepo: UserRepository
  lateinit var commentRepo: CommentRepository
  lateinit var postDataFetcher: DataFetcher

  @Before
  fun setUp() {
    api = mock<Api>()
    postRepo = mock<PostRepository>()
    userRepo = mock<UserRepository>()
    commentRepo = mock<CommentRepository>()
    postDataFetcher = PostDataFetcherImpl(api, postRepo, userRepo, commentRepo)
  }


  @Test
  fun insert_insertListOfUsers_usersFound() {
    runBlocking {
      // PostRepository
      val postList = listOf<Post>()
      val deferredPostList = mock<Deferred<List<Post>>>()
      `when`(deferredPostList.isCompleted).thenReturn(true)
      `when`(deferredPostList.await()).thenReturn(postList)
      `when`(api.fetchPostList()).thenReturn(deferredPostList)
      // UserRepository
      val userList = listOf<User>()
      val deferredUserList = mock<Deferred<List<User>>>()
      `when`(deferredUserList.isCompleted).thenReturn(true)
      `when`(deferredUserList.await()).thenReturn(userList)
      `when`(api.fetchUserList()).thenReturn(deferredUserList)
      // CommentRepository
      val commentList = listOf<Comment>()
      val deferredCommentList = mock<Deferred<List<Comment>>>()
      `when`(deferredCommentList.isCompleted).thenReturn(true)
      `when`(deferredCommentList.await()).thenReturn(commentList)
      `when`(api.fetchCommentList()).thenReturn(deferredCommentList)
      postDataFetcher.fetchData()
      verify(api).fetchPostList()
      verify(api).fetchUserList()
      verify(api).fetchCommentList()
      verify(postRepo).insert(postList)
      verify(userRepo).insert(userList)
      verify(commentRepo).insert(commentList)
    }
  }

}