package uk.co.massimocarli.babylontest.repository

import androidx.lifecycle.*
import java.util.concurrent.Semaphore
import java.util.concurrent.TimeUnit

fun <T> LiveData<T>.waitForValue(): T? {
  var returnValue: T? = null
  val semaphore = Semaphore(0)
  val observer = Observer<T> { t ->
    returnValue = t
    semaphore.release()
  }
  observeForever(observer)
  semaphore.tryAcquire(1, TimeUnit.SECONDS)
  removeObserver(observer)
  return returnValue
}


fun <T> LiveData<T>.observeForTest(onChangeHandler: (T) -> Unit) {
  val observer = TestObserver(handler = onChangeHandler)
  observe(observer, observer)
}


class TestObserver<T>(private val handler: (T) -> Unit) : Observer<T>, LifecycleOwner {

  private val lifecycle = LifecycleRegistry(this)

  init {
    lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
  }

  override fun getLifecycle(): Lifecycle = lifecycle

  override fun onChanged(t: T) {
    handler(t)
    lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
  }
}