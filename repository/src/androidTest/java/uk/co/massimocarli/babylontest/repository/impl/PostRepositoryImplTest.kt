package uk.co.massimocarli.babylontest.repository.impl

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import uk.co.massimocarli.babylontest.repository.PostRepository
import uk.co.massimocarli.babylontest.repository.db.PostDB
import uk.co.massimocarli.babylontest.repository.db.entity.Post
import uk.co.massimocarli.babylontest.repository.waitForValue


@RunWith(AndroidJUnit4::class)
class PostRepositoryImplTest {

  @get:Rule
  var instantTaskExecutorRule = InstantTaskExecutorRule()

  private lateinit var db: PostDB
  private lateinit var postRepository: PostRepository

  companion object {
    val POST1_1 = Post(1, 1, "Title1", "Body1 User 1")
    val POST1_2 = Post(2, 1, "Title2", "Body2 User 1")
    val POST2_1 = Post(3, 2, "Title3", "Body1 User 2")
    val POST2_2 = Post(4, 2, "Title4", "Body2 User 2")
  }

  @Before
  fun setUp() {
    val context = ApplicationProvider.getApplicationContext<Context>()
    db = Room.inMemoryDatabaseBuilder(context, PostDB::class.java)
      .allowMainThreadQueries() // Just here for testing
      .build()
    postRepository = PostRepositoryImpl(db)
  }

  @After
  fun tearDown() {
    db.close()
  }

  @Test
  fun insert_insertListOfUsers_usersFound() {
    runBlocking {
      postRepository.insert(listOf(POST1_1, POST1_2))
      val post1 = postRepository.findById(POST1_1.id).waitForValue()
      Truth.assertThat(post1).isEqualTo(POST1_1)
      val post2 = postRepository.findById(POST1_2.id).waitForValue()
      Truth.assertThat(post2).isEqualTo(POST1_2)
    }
  }

  @Test
  fun insert_insertNothing_usersNotFound() {
    runBlocking {
      val post1 = postRepository.findById(POST1_1.id).waitForValue()
      Truth.assertThat(post1).isNull()
      val post2 = postRepository.findById(POST1_2.id).waitForValue()
      Truth.assertThat(post2).isNull()
    }
  }

  @Test
  fun insert_insertListOfUsers_listFound() {
    runBlocking {
      postRepository.insert(listOf(POST1_1, POST1_2))
      val posts = postRepository.list().waitForValue()
      Truth.assertThat(posts).contains(POST1_1)
      Truth.assertThat(posts).contains(POST1_2)
    }
  }

}