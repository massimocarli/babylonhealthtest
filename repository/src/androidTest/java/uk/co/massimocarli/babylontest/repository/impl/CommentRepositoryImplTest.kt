package uk.co.massimocarli.babylontest.repository.impl

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import uk.co.massimocarli.babylontest.repository.CommentRepository
import uk.co.massimocarli.babylontest.repository.db.PostDB
import uk.co.massimocarli.babylontest.repository.db.entity.Comment

@RunWith(AndroidJUnit4::class)
class CommentRepositoryImplTest {

  private lateinit var db: PostDB
  private lateinit var commentRepository: CommentRepository

  companion object {
    val COMMENT1_1 = Comment(1, 1, "CommentName", "CommentEmail", "BODY")
    val COMMENT1_2 = Comment(1, 2, "CommentName2", "CommentEmail2", "BODY2")
    val COMMENT2_1 = Comment(2, 3, "CommentName3", "CommentEmail3", "BODY3")
    val COMMENT2_2 = Comment(2, 4, "CommentName4", "CommentEmail4", "BODY4")
  }

  @Before
  fun setUp() {
    val context = ApplicationProvider.getApplicationContext<Context>()
    db = Room.inMemoryDatabaseBuilder(context, PostDB::class.java)
      .allowMainThreadQueries() // Just here for testing
      .build()
    commentRepository = CommentRepositoryImpl(db)
  }

  @After
  fun tearDown() {
    db.close()
  }

  @Test
  fun insert_insertListOfCommentsSamePost_commentsFound() {
    runBlocking {
      commentRepository.insert(listOf(COMMENT1_1, COMMENT1_2))
      val comments = commentRepository.findByPostId(1)
      Truth.assertThat(comments).contains(COMMENT1_1)
      Truth.assertThat(comments).contains(COMMENT1_2)
      Truth.assertThat(comments).doesNotContain(COMMENT2_1)
      Truth.assertThat(comments).doesNotContain(COMMENT2_2)
    }
  }

  @Test
  fun insert_insertListOfCommentsForDifferentPost_commentsFound() {
    runBlocking {
      commentRepository.insert(listOf(COMMENT1_1, COMMENT2_1))
      val comments = commentRepository.findByPostId(1)
      Truth.assertThat(comments).contains(COMMENT1_1)
      Truth.assertThat(comments).doesNotContain(COMMENT1_2)
      Truth.assertThat(comments).doesNotContain(COMMENT2_1)
      Truth.assertThat(comments).doesNotContain(COMMENT2_2)
    }
  }

  @Test
  fun insert_insertNoComments_commentsNotFound() {
    runBlocking {
      val comments = commentRepository.findByPostId(1)
      Truth.assertThat(comments).doesNotContain(COMMENT1_1)
      Truth.assertThat(comments).doesNotContain(COMMENT1_2)
      Truth.assertThat(comments).doesNotContain(COMMENT2_1)
      Truth.assertThat(comments).doesNotContain(COMMENT2_2)
    }
  }

  @Test
  fun insert_insertAllComments_commentsNotFound() {
    runBlocking {
      commentRepository.insert(listOf(COMMENT1_1, COMMENT1_2, COMMENT2_1, COMMENT2_2))
      val comments = commentRepository.findByPostId(4)
      Truth.assertThat(comments.size).isEqualTo(0)
      Truth.assertThat(comments).doesNotContain(COMMENT1_1)
      Truth.assertThat(comments).doesNotContain(COMMENT1_2)
      Truth.assertThat(comments).doesNotContain(COMMENT2_1)
      Truth.assertThat(comments).doesNotContain(COMMENT2_2)
    }
  }

  @Test
  fun insert_insertAllComments_commentsAreFound() {
    runBlocking {
      commentRepository.insert(listOf(COMMENT1_1, COMMENT1_2, COMMENT2_1, COMMENT2_2))
      var comments = commentRepository.findByPostId(1)
      Truth.assertThat(comments.size).isEqualTo(2)
      Truth.assertThat(comments).contains(COMMENT1_1)
      Truth.assertThat(comments).contains(COMMENT1_2)
      comments = commentRepository.findByPostId(2)
      Truth.assertThat(comments.size).isEqualTo(2)
      Truth.assertThat(comments).contains(COMMENT2_1)
      Truth.assertThat(comments).contains(COMMENT2_2)
    }
  }

}