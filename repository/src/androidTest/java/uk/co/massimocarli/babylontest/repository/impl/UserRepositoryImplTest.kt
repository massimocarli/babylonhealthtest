package uk.co.massimocarli.babylontest.repository.impl

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import uk.co.massimocarli.babylontest.repository.UserRepository
import uk.co.massimocarli.babylontest.repository.db.PostDB
import uk.co.massimocarli.babylontest.repository.db.entity.Address
import uk.co.massimocarli.babylontest.repository.db.entity.Company
import uk.co.massimocarli.babylontest.repository.db.entity.Geo
import uk.co.massimocarli.babylontest.repository.db.entity.User

@RunWith(AndroidJUnit4::class)
class UserRepositoryImplTest {

  companion object {
    val GEO = Geo(1.0, 2.09)
    val ADDRESS = Address("MyStreet", "Here", "Paperopoli", "123", GEO)
    val COMPANY = Company("Deposito", "Quack", "NO IDEA")
    val TEST_USER_1 = User(1, "Pippo", "UserName", "User@Email.com", ADDRESS, "1234", "www", COMPANY)
    val TEST_USER_2 = User(2, "Mickey", "Mouse", "Topolino@Disney.com", ADDRESS, "777", "xyx", COMPANY)
  }

  private lateinit var db: PostDB
  private lateinit var userRepository: UserRepository

  @Before
  fun setUp() {
    val context = ApplicationProvider.getApplicationContext<Context>()
    db = Room.inMemoryDatabaseBuilder(context, PostDB::class.java)
      .allowMainThreadQueries() // Just here for testing
      .build()
    userRepository = UserRepositoryImpl(db)
  }

  @After
  fun tearDown() {
    db.close()
  }

  @Test
  fun insert_insertListOfUsers_usersFound() {
    runBlocking {
      userRepository.insert(listOf(TEST_USER_1, TEST_USER_2))
      val readUser1 = userRepository.findById(TEST_USER_1.id)
      Truth.assertThat(readUser1).isEqualTo(TEST_USER_1)
      val readUser2 = userRepository.findById(TEST_USER_2.id)
      Truth.assertThat(readUser2).isEqualTo(TEST_USER_2)
    }
  }

  @Test
  fun insert_insertListOfUsersTwice_onlyOneUserFound() {
    runBlocking {
      userRepository.insert(listOf(TEST_USER_1, TEST_USER_1))
      val readUser1 = userRepository.findById(TEST_USER_1.id)
      Truth.assertThat(readUser1).isEqualTo(TEST_USER_1)
      val readUser2 = userRepository.findById(TEST_USER_2.id)
      Truth.assertThat(readUser2).isNull()
    }
  }

  @Test
  fun insert_insertUserTwice_onlyOneUserFound() {
    runBlocking {
      userRepository.insert(listOf(TEST_USER_1))
      userRepository.insert(listOf(TEST_USER_1))
      val readUser1 = userRepository.findById(TEST_USER_1.id)
      Truth.assertThat(readUser1).isEqualTo(TEST_USER_1)
      val readUser2 = userRepository.findById(TEST_USER_2.id)
      Truth.assertThat(readUser2).isNull()
    }
  }

  @Test
  fun list_insertOneUser_onlyOneUserFound() {
    runBlocking {
      userRepository.insert(listOf(TEST_USER_1))
      val users = userRepository.list()
      Truth.assertThat(users.size).isEqualTo(1)
      Truth.assertThat(users[0]).isEqualTo(TEST_USER_1)
    }
  }

  @Test
  fun list_insertTwoUsers_twoUsersFound() {
    runBlocking {
      userRepository.insert(listOf(TEST_USER_1, TEST_USER_2))
      val users = userRepository.list()
      Truth.assertThat(users.size).isEqualTo(2)
      Truth.assertThat(users).contains(TEST_USER_1)
      Truth.assertThat(users).contains(TEST_USER_2)
    }
  }

  @Test
  fun list_insertNothing_listIsEmpty() {
    runBlocking {
      val users = userRepository.list()
      Truth.assertThat(users.size).isEqualTo(0)
      Truth.assertThat(users).isEmpty()
    }
  }
}