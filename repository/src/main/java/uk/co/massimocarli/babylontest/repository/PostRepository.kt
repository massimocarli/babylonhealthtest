package uk.co.massimocarli.babylontest.repository

import androidx.lifecycle.LiveData
import uk.co.massimocarli.babylontest.repository.db.entity.Post

interface PostRepository {

  fun findById(id: Int): LiveData<Post?>

  fun list(): LiveData<List<Post>>

  suspend fun insert(users: List<Post>)
}