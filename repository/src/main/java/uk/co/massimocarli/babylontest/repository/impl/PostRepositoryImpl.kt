package uk.co.massimocarli.babylontest.repository.impl

import androidx.lifecycle.LiveData
import uk.co.massimocarli.babylontest.repository.PostRepository
import uk.co.massimocarli.babylontest.repository.db.PostDB
import uk.co.massimocarli.babylontest.repository.db.entity.Post

/**
 * Implementation for the PostRepository
 */
class PostRepositoryImpl(val db: PostDB) : PostRepository {

  override suspend fun insert(posts: List<Post>) =
    db.getPostDao().insert(posts)

  override fun list(): LiveData<List<Post>> =
    db.getPostDao().list()

  override fun findById(pid: Int): LiveData<Post?> =
    db.getPostDao().findById(pid)
}