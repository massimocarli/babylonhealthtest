package uk.co.massimocarli.babylontest.repository.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import uk.co.massimocarli.babylontest.repository.db.entity.Post

@Dao
abstract class PostDao {

  @Query("SELECT * FROM Post")
  abstract fun list(): LiveData<List<Post>>

  @Query("SELECT * FROM Post WHERE id = :pid")
  abstract fun findById(pid: Int): LiveData<Post?>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  abstract fun insert(user: Post)

  @Transaction
  open fun insert(users: List<Post>) =
    users.forEach {
      insert(it)
    }
}
