package uk.co.massimocarli.babylontest.repository.impl

import uk.co.massimocarli.babylontest.repository.CommentRepository
import uk.co.massimocarli.babylontest.repository.db.PostDB
import uk.co.massimocarli.babylontest.repository.db.entity.Comment

class CommentRepositoryImpl(val db: PostDB) : CommentRepository {

  override suspend fun insert(comments: List<Comment>) =
    db.getCommentDao().insert(comments)

  override fun findByPostId(pid: Int): List<Comment> =
    db.getCommentDao().findByPostId(pid)
}