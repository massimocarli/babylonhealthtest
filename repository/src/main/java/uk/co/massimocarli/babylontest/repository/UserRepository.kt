package uk.co.massimocarli.babylontest.repository

import uk.co.massimocarli.babylontest.repository.db.entity.User

interface UserRepository {

  suspend fun insert(users: List<User>)

  suspend fun findById(uid: Int): User?

  suspend fun list(): List<User>
}