package uk.co.massimocarli.babylontest.repository

import uk.co.massimocarli.babylontest.repository.db.entity.Comment

interface CommentRepository {

  suspend fun insert(comments: List<Comment>)

  fun findByPostId(pid: Int): List<Comment>
}