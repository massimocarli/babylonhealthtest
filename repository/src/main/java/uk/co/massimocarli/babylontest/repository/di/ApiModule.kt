package uk.co.massimocarli.babylontest.repository.di

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import uk.co.massimocarli.babylontest.networking.DataFetcher
import uk.co.massimocarli.babylontest.repository.networking.Api
import uk.co.massimocarli.babylontest.repository.networking.PostDataFetcherImpl
import java.util.concurrent.Executors

val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
val BASE_URL = "https://jsonplaceholder.typicode.com"
val THREAD_POOL_SIZE = 5

val apiModule = module {


  single<Api> {
    val gson = GsonBuilder().setDateFormat(DATE_FORMAT).create()
    val retrofit = Retrofit.Builder()
      .client(get())
      .baseUrl(BASE_URL)
      .callbackExecutor(Executors.newFixedThreadPool(THREAD_POOL_SIZE))
      .addConverterFactory(GsonConverterFactory.create(gson))
      .addCallAdapterFactory(CoroutineCallAdapterFactory())
      .build()
    retrofit.create<Api>(Api::class.java)
  }

  single<DataFetcher> {
    PostDataFetcherImpl(get(), get(), get(), get())
  }
}

