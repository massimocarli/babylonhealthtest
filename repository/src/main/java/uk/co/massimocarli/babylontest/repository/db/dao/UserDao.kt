package uk.co.massimocarli.babylontest.repository.db.dao

import androidx.room.*
import uk.co.massimocarli.babylontest.repository.db.entity.User

@Dao
abstract class UserDao {

  @Query("SELECT * FROM User WHERE id = :uid")
  abstract fun findById(uid: Int): User?

  @Query("SELECT * FROM User")
  abstract fun list(): List<User>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  abstract fun insert(user: User)

  @Transaction
  open fun insert(users: List<User>) =
    users.forEach {
      insert(it)
    }
}
