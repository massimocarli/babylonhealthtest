package uk.co.massimocarli.babylontest.repository.db

import androidx.room.Database
import androidx.room.RoomDatabase
import uk.co.massimocarli.babylontest.repository.db.dao.CommentDao
import uk.co.massimocarli.babylontest.repository.db.dao.PostDao
import uk.co.massimocarli.babylontest.repository.db.dao.UserDao
import uk.co.massimocarli.babylontest.repository.db.entity.Comment
import uk.co.massimocarli.babylontest.repository.db.entity.Post
import uk.co.massimocarli.babylontest.repository.db.entity.User


@Database(entities = arrayOf(User::class, Post::class, Comment::class), version = 1)
abstract class PostDB : RoomDatabase() {

  abstract fun getUserDao(): UserDao

  abstract fun getPostDao(): PostDao

  abstract fun getCommentDao(): CommentDao
}
