package uk.co.massimocarli.babylontest.repository.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Post entity
 */
@Entity
data class Post(
  @PrimaryKey
  val id: Int,
  val userId: Int,
  val title: String,
  val body: String
)
