package uk.co.massimocarli.babylontest.repository.db.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * User entity
 */
@Entity
data class User(
  @PrimaryKey
  val id: Int,
  @ColumnInfo(name = "user_name")
  val name: String,
  val username: String,
  val email: String,
  @Embedded
  val address: Address?,
  val phone: String,
  val website: String,
  @Embedded
  val company: Company?
)

data class Address(
  val street: String,
  val suite: String,
  val city: String,
  val zipcode: String,
  @Embedded
  val geo: Geo?
)

data class Geo(
  val lat: Double,
  val lon: Double
)

data class Company(
  @ColumnInfo(name = "company_name")
  val name: String,
  val catchPhrase: String,
  val bs: String
)
