package uk.co.massimocarli.babylontest.repository.networking

import uk.co.massimocarli.babylontest.networking.DataFetcher
import uk.co.massimocarli.babylontest.repository.CommentRepository
import uk.co.massimocarli.babylontest.repository.PostRepository
import uk.co.massimocarli.babylontest.repository.UserRepository

/**
 * Fetches data from the network and put them into Post DB
 */
class PostDataFetcherImpl(
  val api: Api,
  val postRepo: PostRepository,
  val userRepo: UserRepository,
  val commentRepo: CommentRepository
) : DataFetcher {
  override suspend fun fetchData() {
    // In case of foreign keys this operations have to happen
    // sequentially
    userRepo.insert(api.fetchUserList().await())
    postRepo.insert(api.fetchPostList().await())
    commentRepo.insert(api.fetchCommentList().await())
  }
}