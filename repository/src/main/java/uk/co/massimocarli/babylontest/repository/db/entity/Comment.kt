package uk.co.massimocarli.babylontest.repository.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Comment entity
 */
@Entity
data class Comment(
  val postId: Int,
  @PrimaryKey
  val id: Int,
  val name: String,
  val email: String,
  val body: String
)