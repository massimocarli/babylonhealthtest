package uk.co.massimocarli.babylontest.repository.networking

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import uk.co.massimocarli.babylontest.repository.db.entity.Comment
import uk.co.massimocarli.babylontest.repository.db.entity.Post
import uk.co.massimocarli.babylontest.repository.db.entity.User

interface Api {

  @GET("posts")
  fun fetchPostList(): Deferred<List<Post>>

  @GET("users")
  fun fetchUserList(): Deferred<List<User>>

  @GET("comments")
  fun fetchCommentList(): Deferred<List<Comment>>
}