package uk.co.massimocarli.babylontest.repository.service

import android.app.IntentService
import android.content.Intent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.koin.android.ext.android.inject
import uk.co.massimocarli.babylontest.networking.DataFetcher
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

/**
 * This is an IntentService which launches the
 */
class PostFetchService : IntentService("PostFetchService"), CoroutineScope {

  lateinit var job: Job
  val dataFetcher: DataFetcher by inject()
  override val coroutineContext: CoroutineContext
    get() = job + EmptyCoroutineContext

  override fun onHandleIntent(intent: Intent?) {
    runBlocking {
      dataFetcher.fetchData()
    }
  }

  override fun onCreate() {
    super.onCreate()
    job = Job()
  }

  override fun onDestroy() {
    super.onDestroy()
    job.cancel()
  }
}