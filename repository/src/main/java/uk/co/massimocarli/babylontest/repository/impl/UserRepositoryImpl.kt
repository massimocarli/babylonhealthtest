package uk.co.massimocarli.babylontest.repository.impl

import uk.co.massimocarli.babylontest.repository.UserRepository
import uk.co.massimocarli.babylontest.repository.db.PostDB
import uk.co.massimocarli.babylontest.repository.db.entity.User

class UserRepositoryImpl(val db: PostDB) : UserRepository {

  override suspend fun insert(users: List<User>) =
    db.getUserDao().insert(users)

  override suspend fun findById(uid: Int): User? =
    db.getUserDao().findById(uid)

  override suspend fun list(): List<User> =
    db.getUserDao().list()
}