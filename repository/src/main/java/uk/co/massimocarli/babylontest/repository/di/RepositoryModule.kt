package uk.co.massimocarli.babylontest.repository.di

import androidx.room.Room
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module
import uk.co.massimocarli.babylontest.repository.CommentRepository
import uk.co.massimocarli.babylontest.repository.db.PostDB
import uk.co.massimocarli.babylontest.repository.PostRepository
import uk.co.massimocarli.babylontest.repository.UserRepository
import uk.co.massimocarli.babylontest.repository.impl.CommentRepositoryImpl
import uk.co.massimocarli.babylontest.repository.impl.PostRepositoryImpl
import uk.co.massimocarli.babylontest.repository.impl.UserRepositoryImpl

val repositoryModule = module {

  single<PostRepository> { PostRepositoryImpl(get()) }

  single<CommentRepository> { CommentRepositoryImpl(get()) }

  single<UserRepository> { UserRepositoryImpl(get()) }

  single<PostDB> {
    Room.databaseBuilder(
      androidApplication(),
      PostDB::class.java,
      "post-db"
    ).build()
  }

}
