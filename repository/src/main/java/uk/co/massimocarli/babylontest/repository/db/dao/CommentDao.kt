package uk.co.massimocarli.babylontest.repository.db.dao

import androidx.room.*
import uk.co.massimocarli.babylontest.repository.db.entity.Comment

@Dao
abstract class CommentDao {

  @Query("SELECT * FROM Comment WHERE postId = :pid")
  abstract fun findByPostId(pid: Int): List<Comment>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  abstract fun insert(comment: Comment)

  @Transaction
  open fun insert(users: List<Comment>) =
    users.forEach {
      insert(it)
    }
}
