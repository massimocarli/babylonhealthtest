package uk.co.massimocarli.babylontest

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Rule
import org.junit.Test

class MainActivityTest {

  @get:Rule
  val intentsTestRule = IntentsTestRule(MainActivity::class.java)

  @Test
  fun whenMenuSelected_fetchServiceLaunched() {
    openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().context);
    onView(withText(R.string.menu_item_refresh_label)).perform(click());
    // Unfortunately it looks that intended() doesn't work with startService :(
    // intended(hasComponent(PostFetchService::class.java.name))
  }
}