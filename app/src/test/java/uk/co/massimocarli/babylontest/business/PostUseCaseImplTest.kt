package uk.co.massimocarli.babylontest.business

import com.google.common.truth.Truth
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mockito.`when`
import uk.co.massimocarli.babylontest.repository.CommentRepository
import uk.co.massimocarli.babylontest.repository.UserRepository
import uk.co.massimocarli.babylontest.repository.db.entity.Post
import uk.co.massimocarli.babylontest.repository.db.entity.User

class PostUseCaseImplTest {

  lateinit var postUseCase: PostUseCase
  lateinit var userRepository: UserRepository
  lateinit var commentRepository: CommentRepository

  @Before
  fun setUp() {
    userRepository = mock<UserRepository>()
    commentRepository = mock<CommentRepository>()
    postUseCase = PostUseCaseImpl(userRepository, commentRepository)
  }

  @Test
  fun enhancePostData_whenPostIsNull_returnsNull() {
    runBlocking {
      val result = postUseCase.enhancePostData(null)
      Truth.assertThat(result).isNull()
      verify(userRepository, never()).findById(anyInt())
      verify(commentRepository, never()).findByPostId(anyInt())
    }
  }

  @Test
  fun enhancePostData_whenSinglePostNoComments_returnsSimplePost() {
    runBlocking {
      val post = Post(1, 2, "TITLE", "BODY")
      val user = User(2, "Name", "UserName", "user@mail.com", null, "1234", "www", null)
      `when`(userRepository.findById(2)).thenReturn(user)
      `when`(commentRepository.findByPostId(1)).thenReturn(emptyList())
      val result = postUseCase.enhancePostData(post)
      verify(userRepository).findById(2)
      verify(commentRepository).findByPostId(1)
      Truth.assertThat(result).isNotNull()
      if (result != null) {
        Truth.assertThat(result.body).isEqualTo(post.body)
        Truth.assertThat(result.comments.size).isEqualTo(0)
        Truth.assertThat(result.email).isEqualTo(user.email)
        Truth.assertThat(result.title).isEqualTo(post.title)
        Truth.assertThat(result.username).isEqualTo(user.username)
      }
    }
  }
}