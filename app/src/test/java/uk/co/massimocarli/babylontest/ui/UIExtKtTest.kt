package uk.co.massimocarli.babylontest.ui

import android.content.Context
import com.google.common.truth.Truth
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import uk.co.massimocarli.babylontest.R

class UIExtKtTest {

  companion object {
    const val NO_COMMENT = "NO COMMENT"
    const val ONE_COMMENT = "1 COMMENT"
    const val MANY_COMMENTS = "10 COMMENT"
    const val COMMENT_NUMBER = 10
    const val SHORT_STRING = "asdasdasdas"
    const val LONG_STRING = SHORT_STRING + "jahSJDHLakjdhaslkdjhaskljDJHAslkdjhasJKDHslak"
  }

  lateinit var ctx: Context

  @Before
  fun setUp() {
    ctx = mock<Context>()
  }

  @Test
  fun asCommentsNumber_withNoComments_rightResourceUsed() {
    `when`(ctx.getString(R.string.post_no_comments)).thenReturn(NO_COMMENT)
    val result = 0.asCommentsNumber(ctx)
    Truth.assertThat(result).isEqualTo(NO_COMMENT)
    verify(ctx).getString(R.string.post_no_comments)
    verify(ctx, never()).getString(R.string.post_one_comment)
    verify(ctx, never()).getString(R.string.post_more_comment, COMMENT_NUMBER)
  }

  @Test
  fun asCommentsNumber_withOneComments_rightResourceUsed() {
    `when`(ctx.getString(R.string.post_one_comment)).thenReturn(ONE_COMMENT)
    val result = 1.asCommentsNumber(ctx)
    Truth.assertThat(result).isEqualTo(ONE_COMMENT)
    verify(ctx, never()).getString(R.string.post_no_comments)
    verify(ctx).getString(R.string.post_one_comment)
    verify(ctx, never()).getString(R.string.post_more_comment, COMMENT_NUMBER)
  }

  @Test
  fun asCommentsNumber_withManyComments_rightResourceUsed() {
    `when`(ctx.getString(R.string.post_more_comment, COMMENT_NUMBER)).thenReturn(MANY_COMMENTS)
    val result = COMMENT_NUMBER.asCommentsNumber(ctx)
    Truth.assertThat(result).isEqualTo(MANY_COMMENTS)
    verify(ctx, never()).getString(R.string.post_no_comments)
    verify(ctx, never()).getString(R.string.post_one_comment)
    verify(ctx).getString(R.string.post_more_comment, COMMENT_NUMBER)
  }

  @Test
  fun truncate_stringShorter_returnsTheSame() {
    val result = SHORT_STRING.truncate(100)
    Truth.assertThat(result).isEqualTo(SHORT_STRING)
  }

  @Test
  fun truncate_stringLonger_returnsTruncated() {
    val result = LONG_STRING.truncate(SHORT_STRING.length)
    Truth.assertThat(result).isEqualTo(SHORT_STRING)
  }
}