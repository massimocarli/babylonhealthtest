package uk.co.massimocarli.babylontest.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import uk.co.massimocarli.babylontest.business.PostUseCase
import uk.co.massimocarli.babylontest.ui.model.PostListItem

const val MAX_PREVIEW_LENGTH = 300

class AppRepositoryImpl(
  val postRepository: PostRepository,
  val postUseCase: PostUseCase
) : AppRepository {

  override fun findPostById(pid: Int): LiveData<PostListItem> =
    Transformations.switchMap(postRepository.findById(pid)) { post ->
      val findPostData = MutableLiveData<PostListItem>()
      GlobalScope.launch {
        findPostData.postValue(postUseCase.enhancePostData(post, true))
      }
      findPostData
    }

  override fun list(): LiveData<List<PostListItem>> =
    Transformations.switchMap(postRepository.list()) {
      val newData = MutableLiveData<List<PostListItem>>()
      GlobalScope.launch {
        newData.postValue(it.map { postUseCase.enhancePostData(it)!! })
      }
      newData
    }
}