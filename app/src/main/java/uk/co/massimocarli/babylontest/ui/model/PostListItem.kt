package uk.co.massimocarli.babylontest.ui.model

data class PostListItem(
  val id: Int,
  val title: String,
  val userId: Int,
  val username: String,
  val email: String,
  val body: String,
  val comments: List<CommentListItem>
)