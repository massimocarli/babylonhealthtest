package uk.co.massimocarli.babylontest

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import uk.co.massimocarli.babylontest.repository.service.PostFetchService
import uk.co.massimocarli.babylontest.ui.navigation.Navigation
import uk.co.massimocarli.babylontest.ui.features.postlist.MainFragment


class MainActivity : AppCompatActivity(), Navigation {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    if (savedInstanceState == null) {
      replaceFragment(MainFragment())
    }
    fetchData()
  }


  override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    MenuInflater(this).inflate(R.menu.main_menu, menu)
    return super.onCreateOptionsMenu(menu)
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      R.id.refresh_menu_item -> fetchData()
      else -> {
        // Nope
      }
    }
    return super.onOptionsItemSelected(item)
  }

  private fun fetchData() {
    Intent(this, PostFetchService::class.java).apply {
      startService(this)
    }
  }

  override fun replaceFragment(
    fragment: Fragment,
    backStackName: String?,
    tag: String?
  ) {
    supportFragmentManager.beginTransaction().apply {
      replace(R.id.anchor, fragment)
      backStackName?.let {
        addToBackStack(it)
      }
      commit()
    }
  }

  override fun back() {
    supportFragmentManager.popBackStack()
  }
}