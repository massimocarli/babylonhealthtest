package uk.co.massimocarli.babylontest.di

import org.koin.dsl.module.module
import uk.co.massimocarli.babylontest.business.CachedPostUseCaseDecorator
import uk.co.massimocarli.babylontest.business.PostUseCase
import uk.co.massimocarli.babylontest.business.PostUseCaseImpl
import uk.co.massimocarli.babylontest.repository.AppRepository
import uk.co.massimocarli.babylontest.repository.AppRepositoryImpl

val appModule = module {

  single<AppRepository> { AppRepositoryImpl(get(), get()) }

  single<PostUseCase> { CachedPostUseCaseDecorator(PostUseCaseImpl(get(), get())) }
}

