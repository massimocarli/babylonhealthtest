package uk.co.massimocarli.babylontest.business

import android.util.LruCache
import uk.co.massimocarli.babylontest.repository.db.entity.Post
import uk.co.massimocarli.babylontest.ui.model.PostListItem

class CachedPostUseCaseDecorator(
  val decoratee: PostUseCase,
  maxSize: Int = 10000
) : PostUseCase {

  val cache = LruCache<Pair<Int, Boolean>, PostListItem?>(maxSize)

  override suspend fun enhancePostData(post: Post?, bodyIsShort: Boolean): PostListItem? {
    if (post != null) {
      val key = post.id to bodyIsShort
      var cachedItem = cache.get(key)
      if (cachedItem == null) {
        cachedItem = decoratee.enhancePostData(post, bodyIsShort)?.apply {
          cache.put(key, this)
        }
      }
      return cachedItem
    }
    return null
  }
}