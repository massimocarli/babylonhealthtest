package uk.co.massimocarli.babylontest.ui.features.postdetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uk.co.massimocarli.babylontest.R
import uk.co.massimocarli.babylontest.ui.model.CommentListItem

typealias OnViewHolderClickListener<T> = (T) -> Unit

class CommentViewAdapter(
  val model: List<CommentListItem>
) : RecyclerView.Adapter<CommentViewHolder>() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
    val view = LayoutInflater.from(parent.context)
      .inflate(R.layout.comment_item_layout, parent, false)
    return CommentViewHolder(view)
  }

  override fun getItemCount(): Int = model.size

  override fun onBindViewHolder(holder: CommentViewHolder, position: Int) =
    holder.bindModel(model[position])
}