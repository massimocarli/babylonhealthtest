package uk.co.massimocarli.babylontest.ui.features.postdetail

import androidx.recyclerview.widget.DiffUtil
import uk.co.massimocarli.babylontest.ui.model.CommentListItem
import java.util.*

class CommentDiffCallback(
  val newComments: List<CommentListItem>,
  val oldComments: List<CommentListItem>
) : DiffUtil.Callback() {

  override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
    oldComments[oldItemPosition].id == newComments[newItemPosition].id

  override fun getOldListSize(): Int = oldComments.size

  override fun getNewListSize(): Int = newComments.size

  override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
    Objects.equals(oldComments[oldItemPosition], newComments[oldItemPosition])
}