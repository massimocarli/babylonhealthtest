package uk.co.massimocarli.babylontest.repository

import androidx.lifecycle.LiveData
import uk.co.massimocarli.babylontest.ui.model.PostListItem

interface AppRepository {

  fun list(): LiveData<List<PostListItem>>

  fun findPostById(pid: Int): LiveData<PostListItem>
}