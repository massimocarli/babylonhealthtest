package uk.co.massimocarli.babylontest.ui.features.postlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uk.co.massimocarli.babylontest.R
import uk.co.massimocarli.babylontest.ui.model.PostListItem

typealias OnViewHolderClickListener<T> = (T) -> Unit

class PostViewAdapter(
  val model: List<PostListItem>,
  val listener: OnViewHolderClickListener<PostListItem>? = null
) : RecyclerView.Adapter<PostViewHolder>() {
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
    val view = LayoutInflater.from(parent.context)
      .inflate(R.layout.post_item_layout, parent, false)
    return PostViewHolder(view, listener)
  }

  override fun getItemCount(): Int = model.size

  override fun onBindViewHolder(holder: PostViewHolder, position: Int) =
    holder.bindModel(model[position])

}