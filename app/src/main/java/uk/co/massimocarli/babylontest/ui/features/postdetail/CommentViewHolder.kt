package uk.co.massimocarli.babylontest.ui.features.postdetail

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.comment_item_layout.view.*
import uk.co.massimocarli.babylontest.ui.model.CommentListItem

class CommentViewHolder(
  val commentView: View
) : RecyclerView.ViewHolder(commentView) {

  val ctx: Context

  init {
    ctx = commentView.context
  }

  fun bindModel(model: CommentListItem) {
    with(commentView) {
      commentAuthorAndEmail.text = model.name
      commentBody.text = model.body
    }
  }
}
