package uk.co.massimocarli.babylontest.ui.features.postdetail


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_detail.*
import org.koin.android.ext.android.inject
import uk.co.massimocarli.babylontest.R
import uk.co.massimocarli.babylontest.repository.AppRepository
import uk.co.massimocarli.babylontest.ui.model.CommentListItem
import uk.co.massimocarli.babylontest.ui.model.PostListItem
import uk.co.massimocarli.babylontest.ui.viewmodel.PostViewModel
import uk.co.massimocarli.babylontest.ui.viewmodel.PostViewModelFactory

private const val ARG_POST_ID = "postId"

class DetailFragment : Fragment() {

  private lateinit var postViewModel: PostViewModel
  val appRepository: AppRepository by inject()
  private var postId: Int = -1
  private lateinit var adapter: CommentViewAdapter
  private val commentModel: MutableList<CommentListItem> = mutableListOf()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      postId = it.getInt(ARG_POST_ID)
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_detail, container, false)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    activity?.run {
      postViewModel = ViewModelProviders.of(
        this,
        PostViewModelFactory(appRepository)
      ).get(PostViewModel::class.java)
    }
    postViewModel.getPostDetailLiveData(postId)
      .observe(this, Observer {
        displayPostDetail(it)
      })
    commentRecyclerView.layoutManager = LinearLayoutManager(activity)
    val dividerItemDecoration = DividerItemDecoration(
      context, DividerItemDecoration.VERTICAL
    )
    commentRecyclerView.addItemDecoration(dividerItemDecoration)
    adapter = CommentViewAdapter(commentModel)
    commentRecyclerView.adapter = adapter
  }

  private fun displayPostDetail(postDetail: PostListItem) {
    // This could be done using Data Binding.
    postTitle.text = postDetail.title
    postAuthorAndEmail.text =
      getString(R.string.post_author_email_format, postDetail.username, postDetail.email)
    activity?.let {
      postCommentNumber.text = getString(
        R.string.post_comments_label,
        postDetail.comments.size
      )
    }
    postBody.text = postDetail.body
    updateComments(postDetail.comments)
  }

  private fun updateComments(newModel: List<CommentListItem>) {
    val diffResult = DiffUtil.calculateDiff(
      CommentDiffCallback(
        commentModel,
        newModel
      )
    )
    diffResult.dispatchUpdatesTo(adapter)
    commentModel.clear();
    commentModel.addAll(newModel);
  }

  companion object {
    @JvmStatic
    fun newInstance(postId: Int) =
      DetailFragment().apply {
        arguments = Bundle().apply {
          putInt(ARG_POST_ID, postId)
        }
      }
  }
}
