package uk.co.massimocarli.babylontest.ui.model

data class CommentListItem(
  val id: Int,
  val name: String,
  val body: String
)