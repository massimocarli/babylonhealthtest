package uk.co.massimocarli.babylontest.ui.features.postlist

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.post_item_layout.view.*
import uk.co.massimocarli.babylontest.R
import uk.co.massimocarli.babylontest.ui.asCommentsNumber
import uk.co.massimocarli.babylontest.ui.model.PostListItem

class PostViewHolder(
  val postView: View,
  val listener: OnViewHolderClickListener<PostListItem>? = null
) : RecyclerView.ViewHolder(postView) {

  private lateinit var currentModel: PostListItem
  val ctx: Context

  init {
    ctx = postView.context
    postView.setOnClickListener {
      listener?.invoke(currentModel)
    }
  }

  fun bindModel(model: PostListItem) {
    currentModel = model
    postView.postTitle.text = model.title
    postView.postAuthorAndEmail.text =
      ctx.getString(R.string.post_author_email_format, model.username, model.email)
    postView.postCommentNumber.text = model.comments.size.asCommentsNumber(ctx)
  }
}
