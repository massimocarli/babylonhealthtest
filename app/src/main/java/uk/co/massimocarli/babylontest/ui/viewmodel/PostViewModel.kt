package uk.co.massimocarli.babylontest.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import uk.co.massimocarli.babylontest.repository.AppRepository
import uk.co.massimocarli.babylontest.ui.model.PostListItem

class PostViewModel(val appRepo: AppRepository) : ViewModel() {

  fun getPostLiveData(): LiveData<List<PostListItem>> =
    appRepo.list()

  fun getPostDetailLiveData(pid: Int): LiveData<PostListItem> =
    appRepo.findPostById(pid)
}

class PostViewModelFactory(val appRepository: AppRepository) : ViewModelProvider.Factory {

  override fun <T : ViewModel?> create(modelClass: Class<T>): T =
    modelClass.getConstructor(AppRepository::class.java).newInstance(appRepository)

}