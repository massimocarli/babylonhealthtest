package uk.co.massimocarli.babylontest

import android.app.Application
import org.koin.android.ext.android.startKoin
import uk.co.massimocarli.babylontest.di.appModule
import uk.co.massimocarli.babylontest.networking.di.networkModule
import uk.co.massimocarli.babylontest.repository.di.apiModule
import uk.co.massimocarli.babylontest.repository.di.repositoryModule

/**
 * The Application for the Babylon App
 */
class BabylonApp : Application() {

  override fun onCreate() {
    super.onCreate()
    startKoin(this, listOf(appModule, networkModule, apiModule, repositoryModule))
  }

}