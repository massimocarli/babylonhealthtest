package uk.co.massimocarli.babylontest.business

import uk.co.massimocarli.babylontest.repository.CommentRepository
import uk.co.massimocarli.babylontest.repository.MAX_PREVIEW_LENGTH
import uk.co.massimocarli.babylontest.repository.UserRepository
import uk.co.massimocarli.babylontest.repository.db.entity.Post
import uk.co.massimocarli.babylontest.ui.model.CommentListItem
import uk.co.massimocarli.babylontest.ui.model.PostListItem
import uk.co.massimocarli.babylontest.ui.truncate

class PostUseCaseImpl(
  val userRepository: UserRepository,
  val commentRepository: CommentRepository
) : PostUseCase {

  suspend override fun enhancePostData(post: Post?, bodyIsShort: Boolean): PostListItem? {
    post?.let {
      val user = userRepository.findById(post.userId)
      val username = user?.username ?: "-"
      val email = user?.email ?: "-"
      val comments = commentRepository.findByPostId(post.id).map { comment ->
        CommentListItem(comment.id, comment.name, comment.body)
      }
      return PostListItem(
        post.id,
        post.title,
        post.userId,
        username,
        email,
        if (bodyIsShort) post.body.truncate(MAX_PREVIEW_LENGTH) else post.body,
        comments
      )
    }
    return null
  }
}