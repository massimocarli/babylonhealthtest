package uk.co.massimocarli.babylontest.ui.features.postlist

import androidx.recyclerview.widget.DiffUtil
import uk.co.massimocarli.babylontest.ui.model.PostListItem
import java.util.*

class PostDiffCallback(
  val newPosts: List<PostListItem>,
  val oldPosts: List<PostListItem>
) : DiffUtil.Callback() {

  override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
    oldPosts[oldItemPosition].id == newPosts[newItemPosition].id

  override fun getOldListSize(): Int = oldPosts.size

  override fun getNewListSize(): Int = newPosts.size

  override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
    Objects.equals(oldPosts[oldItemPosition], newPosts[oldItemPosition])
}