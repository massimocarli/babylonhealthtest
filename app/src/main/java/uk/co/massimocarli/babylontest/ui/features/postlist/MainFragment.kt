package uk.co.massimocarli.babylontest.ui.features.postlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.android.ext.android.inject
import uk.co.massimocarli.babylontest.R
import uk.co.massimocarli.babylontest.repository.AppRepository
import uk.co.massimocarli.babylontest.ui.features.postdetail.DetailFragment
import uk.co.massimocarli.babylontest.ui.model.PostListItem
import uk.co.massimocarli.babylontest.ui.navigation.Navigation
import uk.co.massimocarli.babylontest.ui.viewmodel.PostViewModel
import uk.co.massimocarli.babylontest.ui.viewmodel.PostViewModelFactory

class MainFragment : Fragment() {

  private lateinit var adapter: PostViewAdapter
  private lateinit var postViewModel: PostViewModel
  private lateinit var navigation: Navigation

  private val postModel: MutableList<PostListItem> = mutableListOf()

  val appRepository: AppRepository by inject()

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_main, container, false)
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    val activityAsNavigation = activity as? Navigation
    if (activityAsNavigation != null) {
      navigation = activityAsNavigation
    } else {
      throw IllegalStateException("Navigation Needed!")
    }
    activity?.run {
      postViewModel = ViewModelProviders.of(
        this,
        PostViewModelFactory(appRepository)
      ).get(PostViewModel::class.java)
    }
    postViewModel.getPostLiveData().observe(this, Observer { newPostData ->
      updateModel(newPostData)
    })
    recyclerView.layoutManager = LinearLayoutManager(activity)
    val dividerItemDecoration = DividerItemDecoration(
      context, DividerItemDecoration.VERTICAL
    )
    recyclerView.addItemDecoration(dividerItemDecoration)
    adapter = PostViewAdapter(postModel) { selectedPost ->
      navigation.replaceFragment(
        DetailFragment.newInstance(selectedPost.id),
        "Detail"
      )
    }
    recyclerView.adapter = adapter
  }

  private fun updateModel(newModel: List<PostListItem>) {
    val diffResult = DiffUtil.calculateDiff(
      PostDiffCallback(
        postModel,
        newModel
      )
    )
    diffResult.dispatchUpdatesTo(adapter)
    postModel.clear();
    postModel.addAll(newModel);
  }

}
