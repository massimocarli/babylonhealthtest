package uk.co.massimocarli.babylontest.business

import uk.co.massimocarli.babylontest.repository.db.entity.Post
import uk.co.massimocarli.babylontest.ui.model.PostListItem

interface PostUseCase {

  suspend fun enhancePostData(post: Post?, bodyIsShort: Boolean = false): PostListItem?
}