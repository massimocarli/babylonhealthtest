package uk.co.massimocarli.babylontest.ui

import android.content.Context
import uk.co.massimocarli.babylontest.R

/**
 * Extension method in order to avoid plurals because not available through Context
 */
fun Int.asCommentsNumber(ctx: Context): String = when (this) {
  0 -> ctx.getString(R.string.post_no_comments)
  1 -> ctx.getString(R.string.post_one_comment)
  else -> ctx.getString(R.string.post_more_comment, this)
}

/**
 * Extension function in order to truncate a String
 */
fun String.truncate(length: Int) = if (this.length > length) this.substring(0, length) else this