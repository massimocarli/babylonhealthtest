package uk.co.massimocarli.babylontest.networking

/**
 * Fetcher responsible for getting data from a source (Network) and putting it into
 * a local destination (DB)
 */
interface DataFetcher {

  /**
   * Fetches data from the souce and put them into a local destination
   */
  suspend fun fetchData()
}