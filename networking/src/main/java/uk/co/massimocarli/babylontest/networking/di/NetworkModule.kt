package uk.co.massimocarli.babylontest.networking.di

import okhttp3.Cache
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

val CACHE_SIZE = 10 * 1024 * 1024 // 10 MB

val networkModule = module {

  single<OkHttpClient> {
    val cache = Cache(androidContext().cacheDir, CACHE_SIZE.toLong())
    OkHttpClient.Builder().cache(cache).build()
  }
}

